package service

import (
	"CurrencyService/internal/app/currency/repository"
	"CurrencyService/internal/app/currency/types"
	"context"
)

type Service struct {
	storage *repository.Storage
}

func NewService(storage *repository.Storage) *Service {
	return &Service{storage: storage}
}

func (s *Service) GetPairs(ctx context.Context) ([]types.Pair, error) {
	return s.storage.Сurrency.GetPairs(ctx)
}
