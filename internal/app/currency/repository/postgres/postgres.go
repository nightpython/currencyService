package postgres

import (
	"CurrencyService/internal/app/currency/types"
	"context"
	"github.com/jmoiron/sqlx"
)

type PostgrStorage struct {
	db *sqlx.DB
}

func NewPostgrStorage(db *sqlx.DB) *PostgrStorage {
	return &PostgrStorage{db: db}
}

func (s PostgrStorage) GetPairs(ctx context.Context) (pairs []types.Pair, err error) {
	query := `select currency_from,currency_to, well from public.currency`

	return pairs, s.db.SelectContext(ctx, &pairs, query)
}
