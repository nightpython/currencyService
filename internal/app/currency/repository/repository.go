package repository

import (
	"CurrencyService/internal/app/currency/types"
	"context"
)

type Storage struct {
	//сделать глобальной для экспортируемости?
	Сurrency Currency
}

func NewStorage(currency Currency) *Storage {
	return &Storage{Сurrency: currency}
}

//Currency создали интерфейс и передали его в структуру,
//чтобы использовать конструктор postgres storage в качестве имплементации интерфейса
//(создали дополнительно прослойку отделяющую вызов хранилища от типа БД)
type Currency interface {
	GetPairs(ctx context.Context) ([]types.Pair, error)
}
