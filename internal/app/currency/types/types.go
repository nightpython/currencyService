package types

type Pair struct {
	CurrencyFrom string `json:"currency_from" db:"currency_from"`
	CurrencyTo   string `json:"currency_to" db:"currency_to"`
	Well         int    `json:"well" db:"well"`
}
