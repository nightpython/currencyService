package http

import (
	"github.com/gofiber/fiber/v2"
)

func (s Server) GetCurrency(c *fiber.Ctx) error {
	val, err := s.service.GetPairs(c.Context())
	if err != nil {
		return err
	}
	return c.JSON(val)
}
