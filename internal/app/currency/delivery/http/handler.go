package http

import (
	"CurrencyService/internal/app/currency/service"
	"github.com/gofiber/fiber/v2"
)

type Server struct {
	fiber   *fiber.App
	service *service.Service
}

func NewServer(service *service.Service) *Server {
	server := &Server{
		service: service,
	}
	server.fiber = fiber.New()
	server.setupRoutes()
	return server
}

func (s Server) setupRoutes() {
	s.fiber.Get("/api/currency", s.GetCurrency)
}

func (s Server) Listen(addr string) error {
	err := s.fiber.Listen(addr)
	return err
}
