create table if not exists currency
(
    currency_from varchar(255) not null,
    currency_to varchar(255) not null,
    well int not null,
    updated_at timestamp not null default now()
);