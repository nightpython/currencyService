module CurrencyService

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.35.0
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/jackc/pgx/v4 v4.16.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
)
