package controllers

import (
	"CurrencyService/server"
	"github.com/jmoiron/sqlx"
)

type CurrencyQueries struct {
	*sqlx.DB
}

func (c *CurrencyQueries) GetList() ([]server.Currency, error) {
	// Define list variable.
	list := []server.Currency{}

	// Define query string.
	query := `SELECT * FROM postgres.public.currency`

	// Send query to database.
	err := c.Get(&list, query)
	if err != nil {
		// Return empty object and error.
		return list, err
	}

	// Return query result.
	return list, nil
}
