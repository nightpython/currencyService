package server

type Currency struct {
	CurrencyFrom string `json:"currency_from"`
	CurrencyTo   string `json:"currency_to"`
	Well         int    `json:"well"`
}
