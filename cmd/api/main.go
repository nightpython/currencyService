package main

import (
	_ "github.com/lib/pq"
	"log"
)

func main() {

	//tx := db.MustBegin()
	//tx.MustExec("INSERT INTO postgres.public.currency (currency_from, currency_to, well, updated_at) VALUES ($1, $2, $3, $4)", "USD", "RUB", "75", "2020-09-23 09:13:00+00")
	//tx.Commit()

	//запуск сервера
	if err := app(); err != nil {
		log.Fatalln(err)
	}
}
