package main

import (
	"CurrencyService/internal/app/currency/delivery/http"
	"CurrencyService/internal/app/currency/repository"
	"CurrencyService/internal/app/currency/repository/postgres"
	"CurrencyService/internal/app/currency/service"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func app() error {

	db, err := sqlx.Open("postgres", "user=user dbname=postgres password=pass sslmode=disable")
	if err != nil {
		return err
	}
	defer db.Close()

	postgreRepo := postgres.NewPostgrStorage(db)
	repo := repository.NewStorage(postgreRepo)
	serviceLayer := service.NewService(repo)
	handler := http.NewServer(serviceLayer)

	err = handler.Listen(":8080")

	return err
}
